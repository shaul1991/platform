@php

    @endphp

@extends('layouts.app')

@section('title')

@endsection

@push('css')

@endpush

@section('content')
    <div class="container mx-auto">
        <form action="{{ route('boards.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <hr>
            <label>
                제목 :
                <input type="text" name="subject">
            </label>
            <hr>
            <label>
                내용 :
                <textarea name="contents" cols="30" rows="10"></textarea>
            </label>
            <hr>
            <label>
                파일 업로드1 :
                <input type="file" name="files[]">
            </label>
            <hr>
            <label>
                파일 업로드2 :
                <input type="file" name="files[]">
            </label>
            <hr>
            <label>
                비공개 여부 :
                <input type="hidden" name="is_private" value="{{ \App\Models\Board::VIEW_PUBLIC }}" checked>
                <input type="checkbox" name="is_private" value="{{ \App\Models\Board::VIEW_PRIVATE }}">
            </label>
            <hr>
            <button type="submit">등록</button>
        </form>
    </div>
@endsection

@push('js')

@endpush
