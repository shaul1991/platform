@php

    @endphp

@extends('layouts.app')

@section('title')

@endsection

@push('css')

@endpush

@section('content')
    <div class="container mx-auto">
        <form action="{{ route('boards.excel') }}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="file" name="excel">
            <hr>
            <button type="submit">등록</button>
        </form>
    </div>
@endsection

@push('js')

@endpush
