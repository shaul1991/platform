@php
    /**
    * @var \App\Models\Board $board
    */
@endphp

@extends('layouts.app')

@section('title')

@endsection

@push('css')

@endpush

@section('content')
    <div class="container mx-auto">
        <div class="max-w-md mx-auto bg-white rounded-xl shadow-md overflow-hidden md:max-w-2xl">
            <div class="md:flex">
                <div class="md:flex-shrink-0">
                    <img class="h-48 w-full object-cover md:w-48" src="#" alt="Man looking at item at a store">
                </div>
                <div class="p-8">
                    <div class="uppercase tracking-wide text-sm text-indigo-500 font-semibold">
                        {{ $board->subject }}
                    </div>
                    <p class="block mt-1 text-lg leading-tight font-medium text-black">
                        {{ $board->contents }}
                    </p>
                    @foreach($board->files as $file)
                        <p class="mt-2 text-gray-500">
                            {{ $loop->iteration }}.
                            <a href="#">
                                {{ $file->origin_name }}
                            </a>
                        </p>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')

@endpush
