@php
    /**
    * @var \App\Models\Board[] $boards
    */
@endphp

@extends('layouts.app')

@section('title')

@endsection

@push('css')

@endpush

@section('content')
    <div class="container mx-auto">
        <table class="table-auto">
            <thead>
            <tr>
                <th class="w-1/12">번호</th>
                <th>제목</th>
                <th class="w-1/12">작성자</th>
                <th class="w-1/12">조회수</th>
                <th class="w-1/12">작성일시</th>
            </tr>
            </thead>
            <tbody>
            @forelse($boards as $board)
                <tr class="text-center">
                    <td>{{ $board->id }}</td>
                    <td>
                        <a href="{{ route('boards.show', $board) }}">
                            {{ $board->subject }}
                        </a>
                    </td>
                    <td>{{ $board->user->name }}</td>
                    <td>{{ $board->viewCount() }}</td>
                    <td>{{ $board->created_at->diffForHumans() }}</td>
                </tr>
            @empty
                <tr class="cursor-pointer">
                    <td class="w-1" colspan="3">not Items</td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
            <tr>
                <td colspan="5">
                    @if($boards->hasPages())
                        {{ $boards->links() }}
                    @endif
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
@endsection

@push('js')

@endpush
