@php
    use App\Models\User;

    /**
    * @var User $user
    */
@endphp

@component('mail::message')
# {{ $user->name }}님, 회원가입을 환영합니다.

안녕하세요.

@component('mail::button', ['url' => '/'])
플랫폼 가기
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
