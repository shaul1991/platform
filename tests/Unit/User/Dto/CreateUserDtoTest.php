<?php

declare(strict_types=1);


namespace Tests\Unit\User\Dto;


use App\Models\User;
use App\Services\User\Dto\CreateUserDto;
use Carbon\Carbon;
use Hash;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateUserDtoTest extends TestCase
{
    use DatabaseMigrations;
    use WithFaker;

    /**
     * @test
     */
    public function createUserDtoTest()
    {
        $this->withoutExceptionHandling();

        $name = $this->faker->name;
        $email = $this->faker->unique()->safeEmail;
        $password = $this->faker->password(8);

        $createUserDto = (new CreateUserDto(
            $name,
            $email,
            $password
        ));

        $createUserDtoArray = $createUserDto->toArray();

        $this->assertEquals($name, $createUserDtoArray['name']);
        $this->assertEquals($email, $createUserDtoArray['email']);
        $this->assertTrue(Hash::check($password, $createUserDto->getPassword()));
        $this->assertEquals(User::REGISTER_TYPE_ANONYMOUS, $createUserDtoArray['register_type']);
        $this->assertEquals(User::RULE_NORMAL, $createUserDtoArray['rule']);
        $this->assertEquals(User::ACCOUNT_STATUS_NORMAL, $createUserDtoArray['account_status']);

        $this->assertEquals((new User)->defaultProfilePhotoUrl(), $createUserDtoArray['profile_photo_path']);
        $this->assertNull($createUserDtoArray['phone_number']);
        $this->assertEquals(
            Carbon::parse($createUserDtoArray['created_at'])->addYears(User::DORMANCY_YEAR)->format('Y-m-d'),
            Carbon::parse($createUserDtoArray['dormancy_at'])->format('Y-m-d')
        );
    }

}
