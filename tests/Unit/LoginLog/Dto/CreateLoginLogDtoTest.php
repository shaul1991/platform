<?php

declare(strict_types=1);

namespace Tests\Unit\LoginLog\Dto;

use App\Services\LoginLog\Dto\CreateLoginLogDto;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Jenssegers\Agent\Agent;
use Tests\TestCase;

class CreateLoginLogDtoTest extends TestCase
{
    use DatabaseMigrations;
    use WithFaker;

    /**
     * @test
     */
    public function createLoginLogDtoTest()
    {
        $this->withoutExceptionHandling();

        $agent = new Agent();
        $agent->setUserAgent($this->faker->userAgent);

        $ip = $this->faker->ipv4;

        $createLoginLogDto = new CreateLoginLogDto(
            $ip,
            $agent
        );

        $createLoginLogDtoArray = $createLoginLogDto->toArray();

        $this->assertEquals($ip, $createLoginLogDtoArray['ip']);
        $this->assertEquals($agent->isMobile(), $createLoginLogDtoArray['is_mobile']);
        $this->assertEquals($agent->getUserAgent(), $createLoginLogDtoArray['client_agent']);
        $this->assertEquals($agent->browser(), $createLoginLogDtoArray['browser']);
    }
}
