<?php

declare(strict_types=1);


namespace Tests\Feature\Http\User;


use App\Models\LoginLog;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use DatabaseMigrations;
    use WithFaker;

    /**
     * @test
     */
    public function registerTest()
    {
        $this->withoutExceptionHandling();

        $name = $this->faker->name;
        $email = $this->faker->unique()->safeEmail;
        $password = $this->faker->password(8);

        $request = [
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'password_confirmation' => $password
        ];

        $this->assertDatabaseCount((new User())->getTable(), 0);
        $this->assertDatabaseCount((new LoginLog())->getTable(), 0);

        $this->post(route('register', $request))
            ->assertRedirect();

        $this->assertDatabaseCount((new User())->getTable(), 1);
        $this->assertDatabaseCount((new LoginLog())->getTable(), 1);

        $this->assertDatabaseHas(
            (new User())->getTable(),
            [
                'email' => $email
            ]
        );
    }
}
