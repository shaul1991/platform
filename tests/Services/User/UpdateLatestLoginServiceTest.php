<?php

declare(strict_types=1);

namespace Tests\Services\User;

use App\Models\User;
use App\Services\User\UpdateLatestLoginService;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UpdateLatestLoginServiceTest extends TestCase
{
    use DatabaseMigrations;
    use WithFaker;

    /**
     * @test
     */
    public function updateLatestLoginServiceTest()
    {
        $this->withoutExceptionHandling();

        /** @var UpdateLatestLoginService $updateLatestLoginService */
        $updateLatestLoginService = app(UpdateLatestLoginService::class);

        $time = now()->subHour();

        /** @var string|null $ip */
        $ip = $this->faker->randomElement([null, $this->faker->unique()->ipv4]);

        $this->assertDatabaseCount((new User())->getTable(), 0);

        /** @var User $user */
        $user = User::factory()->create(
            [
                'latest_login_at' => $time
            ]
        );

        $this->assertEquals($time, $user->latest_login_at);
        $this->assertNotEquals($ip, $user->latest_login_ip);

        $updateLatestLoginService->update($user, $ip);

        $this->assertDatabaseCount((new User())->getTable(), 1);

        $this->assertNotEquals($time, $user->latest_login_at);
        $this->assertEquals($ip, $user->latest_login_ip);
    }
}
