<?php

declare(strict_types=1);

namespace Tests\Services\User;


use App\Models\User;
use App\Services\User\CreateUserService;
use App\Services\User\Dto\CreateUserDto;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateUserServiceTest extends TestCase
{
    use DatabaseMigrations;
    use WithFaker;

    /**
     * @test
     */
    public function createUserServiceTest()
    {
        $this->withoutExceptionHandling();

        $createUserService = app(CreateUserService::class);

        $name = $this->faker->name;
        $email = $this->faker->unique()->safeEmail;
        $password = $this->faker->password(8);

        $createUserDto = new CreateUserDto(
            $name,
            $email,
            $password
        );

        $this->assertDatabaseCount((new User)->getTable(), 0);

        $user = $createUserService->create($createUserDto);

        $this->assertDatabaseCount((new User)->getTable(), 1);

        $this->assertDatabaseHas(
            (new User)->getTable(),
            [
                'email' => $user->email
            ]
        );
    }
}
