<?php

declare(strict_types=1);

namespace Tests\Services\LoginLog;

use App\Models\User;
use App\Services\LoginLog\CreateLoginLogService;
use App\Services\LoginLog\Dto\CreateLoginLogDto;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Jenssegers\Agent\Agent;
use Tests\TestCase;

class CreateLoginLogServiceTest extends TestCase
{
    use DatabaseMigrations;
    use WithFaker;

    /**
     * @test
     */
    public function createLoginLogServiceTest()
    {
        $this->withoutExceptionHandling();

        /** @var CreateLoginLogService $createLoginLogService */
        $createLoginLogService = app(CreateLoginLogService::class);

        /** @var User $user */
        $user = User::factory()->create();

        $agent = new Agent();
        $agent->setUserAgent($this->faker->userAgent);

        $createLoginLogDto = new CreateLoginLogDto(
            $this->faker->ipv4,
            $agent
        );

        $createLoginLogService->create($user, $createLoginLogDto);


        $this->assertTrue(true);
    }
}
