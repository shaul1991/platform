<?php

declare(strict_types=1);

namespace Tests\View\Auth\Register;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class RegisterViewPageTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function 회원가입페이지_노출_정상()
    {
        $this->withoutExceptionHandling();

        $this->get(route('register'))
            ->assertOk()
            ->assertViewIs('auth.register');
    }

    /**
     * @test
     */
    public function 회원가입페이지_노출_불가_로그인된_상태()
    {
        $this->withoutExceptionHandling();

        /** @var User $user */
        $user = User::factory()->create();

        $this->actingAs($user);

        $this->get(route('register'))
            ->assertRedirect();
    }
}
