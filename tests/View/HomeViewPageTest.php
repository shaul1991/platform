<?php

declare(strict_types=1);

namespace Tests\View;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class HomeViewPageTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function 회원가입완료페이지_노출_정상()
    {
        $this->withoutExceptionHandling();

        /** @var User $user */
        $user = User::factory()->create();

        $this->actingAs($user);

        $this->get(route('index'))
            ->assertOk()
            ->assertViewIs('welcome');
    }
}
