DOCKER ?= cd docker &&
DOCKER_COMPOSE ?= docker-compose

.PHONY: start
start: erase build up ## clean current environment, recreate dependencies and spin up again

.PHONY: rebuild
rebuild: start ## same as start

.PHONY: migration
migration: dumpautoload migrate ## run app composer dumpautoload && composer rebuild

.PHONY: erase
erase: ## stop and delete containers, clean volumes.
		$(DOCKER) $(DOCKER_COMPOSE) stop
		$(DOCKER) $(DOCKER_COMPOSE) rm -v -f

.PHONY: build
build: ## build environment and initialize composer and project dependencies
		$(DOCKER) $(DOCKER_COMPOSE) build

.PHONY: up
up: ## spin up environment
		$(DOCKER) $(DOCKER_COMPOSE) up -d
		$(DOCKER) $(DOCKER_COMPOSE) exec --user="www-data" shaul_was composer install --no-interaction

.PHONY: restart
restart: ## spin restart environment
		$(DOCKER) $(DOCKER_COMPOSE) restart

.PHONY: keygen
keygen: ## laravel key generate
		$(DOCKER) $(DOCKER_COMPOSE) exec --user="www-data" shaul_was php artisan key:generate
		$(DOCKER) $(DOCKER_COMPOSE) exec --user="www-data" shaul_was php artisan jwt:secret

.PHONY: bash
bash: ## run app bash
		$(DOCKER) $(DOCKER_COMPOSE) exec --user="www-data" shaul_was bash

.PHONY: env
env: ## copy .env
		cp .env.example .env
		$(DOCKER) cp .env.example .env
		$(DOCKER) cp docker-compose.sample.yml docker-compose.yml

pull:
		git fetch --all && php artisan migrate && composer dumpautoload && php artisan ide-helper:model -WR
