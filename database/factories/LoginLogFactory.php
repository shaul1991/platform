<?php

namespace Database\Factories;

use App\Models\LoginLog;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Jenssegers\Agent\Agent;

class LoginLogFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LoginLog::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $agent = new Agent();
        $agent->setUserAgent($this->faker->userAgent);

        return [
            'user_id' => User::factory(),
            'client_agent' => $agent->getUserAgent(),
            'browser' => $agent->browser(),
            'ip' => $this->faker->ipv4,
            'is_mobile' => $agent->isMobile(),
        ];
    }
}
