<?php

namespace Database\Factories;

use App\Models\Board;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class BoardFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Board::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'user_id' => User::factory(),
            'subject' => $this->faker->sentence,
            'contents' => $this->faker->paragraph,
            'is_private' => $this->faker->boolean,
            'view_count' => $this->faker->numberBetween(),
        ];
    }

    /**
     * @return BoardFactory
     */
    public function public(): BoardFactory
    {
        return $this->state(
            [
                'is_private' => true
            ]
        );
    }

    /**
     * @return BoardFactory
     */
    public function private(): BoardFactory
    {
        return $this->state(
            [
                'is_private' => false
            ]
        );
    }
}
