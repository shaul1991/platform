<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\User;
use Hash;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * @var User $user
     */
    protected $user;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $now = now();

        $phoneNumbers = [
            null,
            $this->faker->numerify("010########"),
            $this->faker->numerify("01########")
        ];

        return [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'password' => Hash::make('password'),
            'register_type' => User::REGISTER_TYPE_EMAIL,
            'account_status' => User::ACCOUNT_STATUS_NORMAL,
            'rule' => User::RULE_NORMAL,
            'dormancy_at' => $now->addYears(User::DORMANCY_YEAR),
            'phone_number' => $this->faker->randomElement($phoneNumbers),
            'profile_photo_path' => (new User)->defaultProfilePhotoUrl(),
            'latest_login_at' => $now,
            'latest_login_ip' => $this->faker->ipv4,
            'email_verified_at' => $this->faker->randomElement([null, $now]),
            'remember_token' => $this->faker->randomElement([null, Str::random(10)]),
        ];
    }

    /**
     * @return UserFactory
     */
    public function systemAdmin(): UserFactory
    {
        return $this->state(
            [
                'rule' => User::RULE_SYSTEM_ADMIN,
            ]
        );
    }

    /**
     * @return UserFactory
     */
    public function admin(): UserFactory
    {
        return $this->state(
            [
                'rule' => User::RULE_ADMIN,
            ]
        );
    }
}
