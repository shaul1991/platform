<?php

namespace Database\Seeders;

use App\Models\User;
use Database\Factories\UserFactory;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var UserFactory $userFactory */
        $userFactory = User::factory();

        /* System-Admin */
        $userFactory
            ->count(5)
            ->systemAdmin()
            ->create();

        /* Admin */
        $userFactory
            ->count(20)
            ->admin()
            ->create();

        /* Normal-User */
        $userFactory
            ->count(100)
            ->create();
    }
}
