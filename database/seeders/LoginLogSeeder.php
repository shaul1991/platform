<?php

namespace Database\Seeders;

use App\Models\LoginLog;
use Database\Factories\LoginLogFactory;
use Illuminate\Database\Seeder;

class LoginLogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var LoginLogFactory $loginLogFactory */
        $loginLogFactory = LoginLog::factory();

        $loginLogFactory
            ->count(100)
            ->create();
    }
}
