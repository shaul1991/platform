<?php

namespace Database\Seeders;

use App\Models\Board;
use Database\Factories\BoardFactory;
use Illuminate\Database\Seeder;

class BoardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var BoardFactory $boardFactory */
        $boardFactory = Board::factory();

        $boardFactory
            ->count(10)
            ->public()
            ->create();

        $boardFactory
            ->count(10)
            ->private()
            ->create();
    }
}
