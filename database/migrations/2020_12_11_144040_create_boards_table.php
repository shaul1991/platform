<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'boards',
            function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('user_id')
                    ->index()
                    ->comment('작성자');
                $table->string('subject')
                    ->comment('제목');
                $table->text('contents')
                    ->comment('내용');
                $table->boolean('is_private')
                    ->default(false)
                    ->comment('비공개 여부');
                $table->unsignedInteger('view_count')
                    ->default(0)
                    ->comment('조회수');
                $table->timestamps();
                $table->softDeletes();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boards');
    }
}
