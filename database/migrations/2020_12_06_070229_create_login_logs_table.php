<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoginLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'login_logs',
            function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('user_id')->comment('유저 id');
                $table->string('ip')->nullable()->comment('로그인 ip');
                $table->boolean('is_mobile')->nullable()->comment('모바일 여부');
                $table->string('browser')->nullable()->index()->comment('client browser 정보');
                $table->string('client_agent')->comment('agent 정보');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('login_logs');
    }
}
