<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'files',
            function (Blueprint $table) {
                $table->id();

                $table->foreignId('user_id')
                    ->comment('등록자');

                $table->foreignId('fileable_id')
                    ->comment('관계 테이블 id');

                $table->string('fileable_type')
                    ->comment('관계 테이블 model명');

                $table->string('category')
                    ->index()
                    ->nullable()
                    ->comment('카테고리');

                $table->string('origin_name')
                    ->comment('파일 원본명');

                $table->string('local_name')
                    ->comment('서버 내 파일명');

                $table->string('mime_type')
                    ->nullable()
                    ->comment('mime 타입');

                $table->unsignedBigInteger('size')
                    ->default(0)
                    ->comment('파일 크기');

                $table->foreignId('deleted_user_id')
                    ->nullable()
                    ->comment('삭제자');

                $table->timestamps();

                $table->softDeletes();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
