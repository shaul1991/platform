<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'users',
            function (Blueprint $table) {
                $table->id();
                $table->string('name')
                    ->comment('이름');
                $table->string('email')
                    ->unique()
                    ->index()
                    ->comment('이메일');
                $table->string('password');
                $table->unsignedSmallInteger('rule')
                    ->default(User::RULE_NORMAL)
                    ->comment('권한 레벨');
                $table->unsignedSmallInteger('account_status')
                    ->default(User::ACCOUNT_STATUS_NORMAL)
                    ->comment('계정 상태');
                $table->unsignedSmallInteger('register_type')
                    ->default(User::REGISTER_TYPE_ANONYMOUS)
                    ->comment('가입 구분');

                $table->timestamp('email_verified_at')
                    ->nullable()
                    ->comment('이메일 가입 승인일');
                $table->string('phone_number')
                    ->nullable()
                    ->comment('휴대폰 번호');
                $table->text('profile_photo_path')
                    ->nullable()
                    ->comment('프로필 이미지');
                $table->timestamp('latest_login_at')
                    ->nullable()
                    ->comment('최근 로그인 날짜');
                $table->ipAddress('latest_login_ip')
                    ->nullable()
                    ->comment('최근 로그인 ip');
                $table->date('dormancy_at')
                    ->nullable()
                    ->index()
                    ->comment('휴면전환일');

                $table->foreignId('current_team_id')->nullable();
                $table->rememberToken();
                $table->timestamps();
                $table->softDeletes();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
