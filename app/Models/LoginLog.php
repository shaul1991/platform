<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\LoginLog
 *
 * @property int $id
 * @property int $user_id 유저 id
 * @property string|null $ip 로그인 ip
 * @property int|null $is_mobile 모바일 여부
 * @property string|null $browser client browser 정보
 * @property string $client_agent agent 정보
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read \App\Models\User $User
 * @method static Builder|LoginLog newModelQuery()
 * @method static Builder|LoginLog newQuery()
 * @method static Builder|LoginLog query()
 * @method static Builder|LoginLog whereBrowser($value)
 * @method static Builder|LoginLog whereClientAgent($value)
 * @method static Builder|LoginLog whereCreatedAt($value)
 * @method static Builder|LoginLog whereId($value)
 * @method static Builder|LoginLog whereIp($value)
 * @method static Builder|LoginLog whereIsMobile($value)
 * @method static Builder|LoginLog whereUpdatedAt($value)
 * @method static Builder|LoginLog whereUserId($value)
 * @mixin Eloquent
 */
class LoginLog extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = [
        'client_agent',
        'ip',
        'browser',
        'is_mobile',
    ];

    /**
     * @return BelongsTo
     */
    public function User(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
