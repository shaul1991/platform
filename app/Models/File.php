<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * App\Models\File
 *
 * @property int $id
 * @property int $user_id 등록자
 * @property int $fileable_id 관계 테이블 id
 * @property string $fileable_type 관계 테이블 model명
 * @property string|null $category 카테고리
 * @property string $origin_name 파일 원본명
 * @property string $local_name 서버 내 파일명
 * @property string|null $mime_type mime 타입
 * @property int $size 파일 크기
 * @property int|null $deleted_user_id 삭제자
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property-read Model|\Eloquent $fileable
 * @property-read \App\Models\User $user
 * @method static Builder|File newModelQuery()
 * @method static Builder|File newQuery()
 * @method static \Illuminate\Database\Query\Builder|File onlyTrashed()
 * @method static Builder|File query()
 * @method static Builder|File whereCategory($value)
 * @method static Builder|File whereCreatedAt($value)
 * @method static Builder|File whereDeletedAt($value)
 * @method static Builder|File whereDeletedUserId($value)
 * @method static Builder|File whereFileableId($value)
 * @method static Builder|File whereFileableType($value)
 * @method static Builder|File whereId($value)
 * @method static Builder|File whereLocalName($value)
 * @method static Builder|File whereMimeType($value)
 * @method static Builder|File whereOriginName($value)
 * @method static Builder|File whereSize($value)
 * @method static Builder|File whereUpdatedAt($value)
 * @method static Builder|File whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|File withTrashed()
 * @method static \Illuminate\Database\Query\Builder|File withoutTrashed()
 * @mixin Eloquent
 */
class File extends Model
{
    use HasFactory;
    use SoftDeletes;

    public const MIME_TYPE_DESCRIPTION = [
        self::MIME_TYPE_ANONYMOUS => "anonymous",
        self::MIME_TYPE_TEXT => "text",
        self::MIME_TYPE_IMAGE => "image",
        self::MIME_TYPE_AUDIO => "audio",
        self::MIME_TYPE_VIDEO => "video",
        self::MIME_TYPE_APPLICATION => "application",
        self::MIME_TYPE_MULTIPART => "multipart",
    ];

    public const MIME_TYPE_ANONYMOUS = 0;
    public const MIME_TYPE_TEXT = 1;
    public const MIME_TYPE_IMAGE = 2;
    public const MIME_TYPE_AUDIO = 3;
    public const MIME_TYPE_VIDEO = 4;
    public const MIME_TYPE_APPLICATION = 5;
    public const MIME_TYPE_MULTIPART = 6;

    public const CATEGORY_TYPE_DESCRIPTION = [
        self::CATEGORY_TYPE_ANONYMOUS => "anonymous",
    ];

    public const CATEGORY_TYPE_ANONYMOUS = 0;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'fileable_id',
        'fileable_type',
        'category',
        'origin_name',
        'local_name',
        'mime_type',
        'size',
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return MorphTo
     */
    public function fileable(): MorphTo
    {
        return $this->morphTo();
    }
}
