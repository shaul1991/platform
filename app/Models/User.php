<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Sanctum\PersonalAccessToken;
use Storage;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name 이름
 * @property string $email 이메일
 * @property string $password
 * @property string|null $two_factor_secret
 * @property string|null $two_factor_recovery_codes
 * @property int $rule 권한 레벨
 * @property int $account_status 계정 상태
 * @property int $register_type 가입 구분
 * @property Carbon|null $email_verified_at 이메일 가입 승인일
 * @property string|null $phone_number 휴대폰 번호
 * @property string|null $profile_photo_path 프로필 이미지
 * @property string|null $latest_login_at 최근 로그인 날짜
 * @property string|null $latest_login_ip 최근 로그인 ip
 * @property string|null $dormancy_at 휴면전환일
 * @property int|null $current_team_id
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property-read Collection|\App\Models\File[] $files
 * @property-read int|null $files_count
 * @property-read string $profile_photo_url
 * @property-read Collection|\App\Models\LoginLog[] $login_log
 * @property-read int|null $login_log_count
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read Collection|PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static \Illuminate\Database\Query\Builder|User onlyTrashed()
 * @method static Builder|User query()
 * @method static Builder|User whereAccountStatus($value)
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereCurrentTeamId($value)
 * @method static Builder|User whereDeletedAt($value)
 * @method static Builder|User whereDormancyAt($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereLatestLoginAt($value)
 * @method static Builder|User whereLatestLoginIp($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User wherePhoneNumber($value)
 * @method static Builder|User whereProfilePhotoPath($value)
 * @method static Builder|User whereRegisterType($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereRule($value)
 * @method static Builder|User whereTwoFactorRecoveryCodes($value)
 * @method static Builder|User whereTwoFactorSecret($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|User withoutTrashed()
 * @mixin Eloquent
 */
class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use SoftDeletes;

    public const REGISTER_TYPE_TEXT = [
        self::REGISTER_TYPE_EMAIL => "이메일",
        self::REGISTER_TYPE_SOCIAL => "소셜",
        self::REGISTER_TYPE_ANONYMOUS => "미확인",
    ];

    public const REGISTER_TYPE_EMAIL = 1;
    public const REGISTER_TYPE_SOCIAL = 2;
    public const REGISTER_TYPE_ANONYMOUS = 99;

    public const ACCOUNT_STATUS_TEXT = [
        self::ACCOUNT_STATUS_WAIT => '대기',
        self::ACCOUNT_STATUS_NORMAL => '정상',
        self::ACCOUNT_STATUS_DORMANCY => '휴면',
        self::ACCOUNT_STATUS_PAUSE => '일시중지',
        self::ACCOUNT_STATUS_WITHDRAW => '탈퇴',
    ];

    public const ACCOUNT_STATUS_WAIT = 1;
    public const ACCOUNT_STATUS_NORMAL = 2;
    public const ACCOUNT_STATUS_DORMANCY = 97;
    public const ACCOUNT_STATUS_PAUSE = 98;
    public const ACCOUNT_STATUS_WITHDRAW = 99;

    public const RULES_TEXT = [
        self::RULE_SYSTEM_ADMIN => "시스템 관리자",
        self::RULE_ADMIN => "관리자",
        self::RULE_NORMAL => "일반 사용자"
    ];

    public const RULE_SYSTEM_ADMIN = 1;
    public const RULE_ADMIN = 2;
    public const RULE_NORMAL = 9;

    // 휴면계정 전환 설정 날짜 : 1년
    public const DORMANCY_YEAR = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'rule',
        'account_status',
        'register_type',
        'phone_number',
        'profile_photo_path',
        'dormancy_at',
        'latest_login_at',
        'latest_login_ip',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    /**
     * @return string
     */
    public function defaultProfilePhotoUrl(): string
    {
        return Storage::disk('icons')->url('user_icon.svg');
    }

    /**
     * @return HasMany
     */
    public function login_log(): HasMany
    {
        return $this->hasMany(LoginLog::class);
    }

    /**
     * @return MorphMany
     */
    public function files(): MorphMany
    {
        return $this->morphMany(File::class, 'fileable');
    }
}
