<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Board
 *
 * @property int $id
 * @property int $user_id 작성자
 * @property string $subject 제목
 * @property string $contents 내용
 * @property int $is_private 비공개 여부
 * @property int $view_count 조회수
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read Collection|\App\Models\File[] $files
 * @property-read int|null $files_count
 * @property-read \App\Models\User $user
 * @method static Builder|Board newModelQuery()
 * @method static Builder|Board newQuery()
 * @method static Builder|Board public()
 * @method static Builder|Board query()
 * @method static Builder|Board whereContents($value)
 * @method static Builder|Board whereCreatedAt($value)
 * @method static Builder|Board whereDeletedAt($value)
 * @method static Builder|Board whereId($value)
 * @method static Builder|Board whereIsPrivate($value)
 * @method static Builder|Board whereSubject($value)
 * @method static Builder|Board whereUpdatedAt($value)
 * @method static Builder|Board whereUserId($value)
 * @method static Builder|Board whereViewCount($value)
 * @mixin Eloquent
 */
class Board extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'subject',
        'contents',
        'is_private',
    ];

    public const VIEW_DESCRIPTION = [
        self::VIEW_PUBLIC => '공개',
        self::VIEW_PRIVATE => '비공개',
    ];

    public const VIEW_PUBLIC = 0;
    public const VIEW_PRIVATE = 1;

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return string
     */
    public function viewCount(): string
    {
        return number_format($this->view_count);
    }

    /**
     * @return MorphMany
     */
    public function files(): MorphMany
    {
        return $this->morphMany(File::class, 'fileable');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopePublic(Builder $query): Builder
    {
        return $query->where('is_private', false);
    }
}
