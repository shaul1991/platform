<?php

declare(strict_types=1);


namespace App\Services\File;


use App\Models\File;
use App\Models\User;
use App\Services\File\Dto\UploadFileDto;
use Illuminate\Http\UploadedFile;

abstract class AbstractUploadFileService
{
    /**
     * @var UploadFileDto
     */
    private $uploadFileDto;

    public function __construct()
    {
        $this->uploadFileDto = new UploadFileDto();
    }

    /**
     * @return File
     */
    public function getFile(): File
    {
        return $this->uploadFileDto->getFile();
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->uploadFileDto->getFileName();
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->uploadFileDto->setUser($user);
    }

    /**
     * @param UploadedFile $file
     * @param string|null $fileName
     */
    public function setFile(UploadedFile $file, string $fileName = null)
    {
        $this->uploadFileDto->setFile($file, $fileName);
    }

    /**
     * @param string $category
     */
    public function setCategory(string $category)
    {
        $this->uploadFileDto->setCategory($category);
    }
}
