<?php

declare(strict_types=1);


namespace App\Services\File\Dto;


use App\Models\File;
use App\Models\User;
use Illuminate\Http\UploadedFile;

class UploadFileDto
{
    /**
     * @var File
     */
    private $file;
    /**
     * @var User
     */
    private $user;

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param string $category
     */
    public function setCategory(string $category)
    {
        $this->file->category = $category;
    }

    /**
     * @return File
     */
    public function getFile(): File
    {
        return $this->file;
    }

    /**
     * @param UploadedFile $file
     * @param string|null $localName
     */
    public function setFile(UploadedFile $file, string $localName = null)
    {
        $this->file = new File();

        $this->file->user_id = $this->user->id;
        $this->file->origin_name = $file->getClientOriginalName();
        $this->file->local_name = $localName ?? $file->hashName();
        $this->file->size = $file->getSize();
        $this->file->mime_type = $file->getMimeType();
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->file->local_name;
    }

}
