<?php

declare(strict_types=1);


namespace App\Services\LoginLog;


use App\Models\User;
use App\Services\LoginLog\Dto\CreateLoginLogDto;
use Illuminate\Database\Eloquent\Model;

class CreateLoginLogService
{
    /**
     * @param User $user
     * @param CreateLoginLogDto $createLoginLogDto
     * @return Model
     */
    public function create(
        User $user,
        CreateLoginLogDto $createLoginLogDto
    ): Model {
        return $user->login_log()->create($createLoginLogDto->toArray());
    }
}
