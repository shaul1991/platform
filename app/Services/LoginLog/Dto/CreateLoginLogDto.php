<?php

declare(strict_types=1);


namespace App\Services\LoginLog\Dto;


use App\Models\LoginLog;
use Jenssegers\Agent\Agent;

class CreateLoginLogDto
{
    /**
     * @var LoginLog
     */
    private $loginLog;

    /**
     * CreateLoginLogDto constructor.
     * @param string|null $ip
     * @param Agent $agent
     */
    public function __construct(?string $ip, Agent $agent)
    {
        $this->loginLog = new LoginLog();

        $this->loginLog->ip = $ip;
        $this->loginLog->browser = $agent->browser();
        $this->loginLog->client_agent = $agent->getUserAgent();
        $this->loginLog->is_mobile = (int)$agent->isMobile();
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->loginLog->toArray();
    }
}
