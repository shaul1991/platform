<?php

declare(strict_types=1);

namespace App\Services\User\Dto;


use App\Models\User;
use Illuminate\Support\Facades\Hash;

class CreateUserDto
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $password;
    /**
     * @var User
     */
    private $user;

    /**
     * CreateUserDto constructor.
     * @param string $name
     * @param string $email
     * @param string $password
     */
    public function __construct(
        string $name,
        string $email,
        string $password
    ) {
        $this->user = new User();
        $this->setPassword($password);
        $now = now();

        $this->user->name = $name;
        $this->user->email = $email;
        $this->user->password = $this->getPassword();

        $this->user->register_type = User::REGISTER_TYPE_ANONYMOUS;
        $this->user->rule = User::RULE_NORMAL;
        $this->user->account_status = User::ACCOUNT_STATUS_NORMAL;

        $this->user->phone_number = null;
        $this->user->profile_photo_path = (new User)->defaultProfilePhotoUrl();
        $this->user->created_at = $now;
        $this->user->updated_at = $now;
        $this->user->dormancy_at = $now->addYears(User::DORMANCY_YEAR)->format('Y-m-d');
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = Hash::make($password);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->user->toArray();
    }
}
