<?php

declare(strict_types=1);


namespace App\Services\User;


use App\Models\User;

class UpdateLatestLoginService
{
    /**
     * @param User $user
     * @param string|null $ip
     * @return bool
     */
    public function update(User $user, ?string $ip): bool
    {
        $user->latest_login_ip = $ip;
        $user->latest_login_at = now()->format('Y-m-d');
        $user->timestamps = false;

        return $user->update();
    }
}
