<?php

declare(strict_types=1);

namespace App\Services\User;


use App\Models\User;
use App\Services\User\Dto\CreateUserDto;

class CreateUserService
{
    /**
     * @param CreateUserDto $dto
     * @return User
     */
    public function create(CreateUserDto $dto): User
    {
        $data = $dto->toArray();
        $data['password'] = $dto->getPassword();

        return User::create($data);
    }
}
