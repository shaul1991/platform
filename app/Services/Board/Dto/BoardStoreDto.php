<?php

declare(strict_types=1);


namespace App\Services\Board\Dto;


class BoardStoreDto
{
    /**
     * @var string
     */
    private $subject;
    /**
     * @var string
     */
    private $contents;
    /**
     * @var bool
     */
    private $is_private;
    /**
     * @var int
     */
    private $user_id;

    /**
     * BoardStoreDto constructor.
     * @param int $userId
     * @param string $subject
     * @param string $contents
     * @param bool $is_private
     */
    public function __construct(int $userId, string $subject, string $contents, bool $is_private)
    {
        $this->user_id = $userId;
        $this->subject = $subject;
        $this->contents = $contents;
        $this->is_private = $is_private;
    }

    /**
     * @return array
     */
    public function get(): array
    {
        return [
            'user_id' => $this->user_id,
            'subject' => $this->subject,
            'contents' => $this->contents,
            'is_private' => $this->is_private,
        ];
    }
}
