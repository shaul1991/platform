<?php

declare(strict_types=1);


namespace App\Services\Board;


use App\Models\Board;
use Illuminate\Database\Eloquent\Builder;

class SearchBoardService
{
    /**
     * @return Board|Builder
     */
    public function public()
    {
        return Board::public();
    }
}
