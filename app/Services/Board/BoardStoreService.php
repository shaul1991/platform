<?php

declare(strict_types=1);


namespace App\Services\Board;


use App\Models\Board;
use App\Services\Board\Dto\BoardStoreDto;
use Illuminate\Database\Eloquent\Model;

class BoardStoreService
{
    /**
     * @var Board
     */
    private $board;

    /**
     * BoardStoreService constructor.
     * @param Board $board
     */
    public function __construct(Board $board)
    {
        $this->board = $board;
    }

    /**
     * @param BoardStoreDto $boardStoreDto
     * @return Board|Model
     */
    public function save(BoardStoreDto $boardStoreDto)
    {
        return Board::create($boardStoreDto->get());
    }
}
