<?php

namespace App\Actions\Fortify;

use App\Models\User;
use App\Services\User\CreateUserService;
use App\Services\User\Dto\CreateUserDto;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * @var CreateUserService
     */
    private $createUserService;

    /**
     * CreateNewUser constructor.
     * @param CreateUserService $createUserService
     */
    public function __construct(CreateUserService $createUserService)
    {
        $this->createUserService = $createUserService;
    }

    /**
     * @param array $input
     * @return User
     * @throws ValidationException
     */
    public function create(array $input): User
    {
        Validator::make(
            $input,
            [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => $this->passwordRules(),
            ]
        )->validate();

        $createUserDto = new CreateUserDto(
            $input['name'],
            $input['email'],
            $input['password']
        );

        return $this->createUserService->create($createUserDto);
    }
}
