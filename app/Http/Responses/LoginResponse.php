<?php

declare(strict_types=1);


namespace App\Http\Responses;


use App\Models\User;
use App\Services\LoginLog\CreateLoginLogService;
use App\Services\LoginLog\Dto\CreateLoginLogDto;
use App\Services\User\UpdateLatestLoginService;
use Jenssegers\Agent\Agent;
use Laravel\Fortify\Contracts\LoginResponse as LoginResponseContract;

class LoginResponse implements LoginResponseContract
{
    /**
     * @var CreateLoginLogService
     */
    private $createLoginLogService;
    /**
     * @var Agent
     */
    private $agent;
    /**
     * @var UpdateLatestLoginService
     */
    private $updateLatestLoginService;

    /**
     * LoginResponse constructor.
     * @param CreateLoginLogService $createLoginLogService
     * @param Agent $agent
     * @param UpdateLatestLoginService $updateLatestLoginService
     */
    public function __construct(
        CreateLoginLogService $createLoginLogService,
        Agent $agent,
        UpdateLatestLoginService $updateLatestLoginService
    ) {
        $this->createLoginLogService = $createLoginLogService;
        $this->agent = $agent;
        $this->updateLatestLoginService = $updateLatestLoginService;
    }

    public function toResponse($request)
    {
        /** @var User $user */
        $user = auth()->user();

        $clientIp = $request->ip();

        $createLoginLogDto = new CreateLoginLogDto(
            $clientIp,
            $this->agent
        );

        $this->updateLatestLoginService->update($user, $clientIp);
        $this->createLoginLogService->create($user, $createLoginLogDto);

        return $request->wantsJson()
            ? response()->json(['two_factor' => false])
            : redirect()->intended(config('fortify.home'));
    }
}
