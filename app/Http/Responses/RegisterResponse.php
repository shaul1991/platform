<?php

declare(strict_types=1);


namespace App\Http\Responses;


use App\Mail\WelcomeMailable;
use App\Models\User;
use App\Services\LoginLog\CreateLoginLogService;
use App\Services\LoginLog\Dto\CreateLoginLogDto;
use App\Services\User\UpdateLatestLoginService;
use Illuminate\Http\JsonResponse;
use Jenssegers\Agent\Agent;
use Laravel\Fortify\Contracts\RegisterResponse as RegisterResponseContract;
use Mail;

class RegisterResponse implements RegisterResponseContract
{
    /**
     * @var UpdateLatestLoginService
     */
    private $updateLatestLoginService;
    /**
     * @var CreateLoginLogService
     */
    private $createLoginLogService;
    /**
     * @var Agent
     */
    private $agent;

    /**
     * RegisterResponse constructor.
     * @param UpdateLatestLoginService $updateLatestLoginService
     * @param CreateLoginLogService $createLoginLogService
     * @param Agent $agent
     */
    public function __construct(
        UpdateLatestLoginService $updateLatestLoginService,
        CreateLoginLogService $createLoginLogService,
        Agent $agent
    ) {
        $this->updateLatestLoginService = $updateLatestLoginService;
        $this->createLoginLogService = $createLoginLogService;
        $this->agent = $agent;
    }


    public function toResponse($request)
    {
        $clientIp = $request->ip();

        $createLoginLogDto = new CreateLoginLogDto(
            $clientIp,
            $this->agent
        );
        /** @var User $user */
        $user = auth()->user();

        $this->updateLatestLoginService->update($user, $clientIp);
        $this->createLoginLogService->create($user, $createLoginLogDto);
        Mail::to($user)->send(new WelcomeMailable($user));

        return $request->wantsJson()
            ? new JsonResponse('', 201)
            : redirect(config('fortify.home'));
    }
}
