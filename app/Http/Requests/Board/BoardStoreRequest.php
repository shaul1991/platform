<?php

namespace App\Http\Requests\Board;

use Illuminate\Foundation\Http\FormRequest;

class BoardStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'subject' => ['required', 'string'],
            'contents' => ['required', 'string'],
            'files' => ['nullable', 'array'],
            'files.*' => ['required', 'file'],
            'is_private' => ['required', 'boolean'],
        ];
    }
}
