<?php

namespace App\Http\Controllers\Board;

use App\Http\Controllers\Controller;
use App\Http\Requests\Board\BoardExcelRequest;
use App\Models\Board;
use App\Services\Board\BoardStoreService;
use App\Services\Board\Dto\BoardStoreDto;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Common\Exception\UnsupportedTypeException;
use Box\Spout\Reader\Exception\ReaderNotOpenedException;
use DB;
use Exception;
use Illuminate\Http\RedirectResponse;
use Rap2hpoutre\FastExcel\FastExcel;
use Throwable;

class BoardExcelController extends Controller
{
    /**
     * @var BoardStoreService
     */
    private $boardStoreService;

    /**
     * BoardExcelController constructor.
     * @param BoardStoreService $boardStoreService
     */
    public function __construct(BoardStoreService $boardStoreService)
    {
        $this->boardStoreService = $boardStoreService;
    }

    public function show()
    {
        return view('boards.excel-show');
    }

    /**
     * @param BoardExcelRequest $request
     * @return RedirectResponse
     * @throws IOException
     * @throws UnsupportedTypeException
     * @throws ReaderNotOpenedException
     * @throws Throwable
     */
    public function store(BoardExcelRequest $request): RedirectResponse
    {
        $excelFile = $request->file('excel');

        $rows = (new FastExcel())->import($excelFile);

        try {
            DB::beginTransaction();
            foreach ($rows as $row) {
                if ($this->isNullValue($row['subject'])) {
                    throw new Exception('제목이 비어있는 행이 있습니다.', 400);
                }

                if ($this->isNullValue($row['contents'])) {
                    throw new Exception('내용이 비어있는 행이 있습니다.', 400);
                }

                if ($this->isNullValue($row['is_private']) || $this->isNotBoolType($row['is_private'])) {
                    $row['is_private'] = Board::VIEW_PUBLIC;
                }

                $boardStoreDto = new BoardStoreDto(
                    $request->user()->id,
                    $row['subject'],
                    $row['contents'],
                    $row['is_private']
                );

                if (!$this->boardStoreService->save($boardStoreDto)) {
                    throw new Exception('글쓰기에 실패하였습니다.', 400);
                }
            }

            DB::commit();

            return redirect()->route('boards.index');
        } catch (Exception $e) {
            DB::rollBack();
            report($e);

            return redirect()->back();
        }
    }

    /**
     * @param $subject
     * @return bool
     */
    private function isNullValue($subject): bool
    {
        return $subject === "";
    }

    /**
     * @param $is_private
     * @return bool
     */
    private function isNotBoolType($is_private): bool
    {
        return !($is_private === 0 || $is_private === 1);
    }
}
