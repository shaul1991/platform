<?php

namespace App\Http\Controllers\Board;

use App\Http\Controllers\Controller;
use App\Http\Requests\Board\BoardStoreRequest;
use App\Models\Board;
use App\Services\Board\BoardStoreService;
use App\Services\Board\BoardUploadService;
use App\Services\Board\Dto\BoardStoreDto;
use App\Services\Board\SearchBoardService;
use DB;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Throwable;

class BoardController extends Controller
{
    /**
     * @var SearchBoardService
     */
    private $searchBoardService;
    /**
     * @var BoardStoreService
     */
    private $boardStoreService;
    /**
     * @var BoardUploadService
     */
    private $boardUploadService;

    /**
     * BoardController constructor.
     * @param SearchBoardService $searchBoardService
     * @param BoardStoreService $boardStoreService
     * @param BoardUploadService $boardUploadService
     */
    public function __construct(
        SearchBoardService $searchBoardService,
        BoardStoreService $boardStoreService,
        BoardUploadService $boardUploadService
    ) {
        $this->searchBoardService = $searchBoardService;
        $this->boardStoreService = $boardStoreService;
        $this->boardUploadService = $boardUploadService;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $boards = $this->searchBoardService->public()->with('user')->paginate(10);

        return view('boards.index', compact('boards'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('boards.create');
    }

    /**
     * @param BoardStoreRequest $request
     * @return RedirectResponse
     * @throws Throwable
     */
    public function store(BoardStoreRequest $request): RedirectResponse
    {
        if (!auth()->check()) {
            throw new Exception('로그인이 필요합니다.', '403');
        }

        try {
            DB::beginTransaction();

            $boardStoreDto = new BoardStoreDto(
                $request->user()->id,
                $request->get('subject'),
                $request->get('contents'),
                $request->get('is_private')
            );

            /** @var Board $board */
            $board = $this->boardStoreService->save($boardStoreDto);
            if (!$board) {
                throw new Exception('게시글이 등록되지 않았습니다.', '500');
            }

            if ($request->hasFile('files')) {
                foreach ($request->file('files') as $file) {
                    /** @var UploadedFile $file */
                    $fileName = $board->getTable() . "_" . $board->id . "_" . $file->hashName();
                    $fileNameWithPath = $file->storePubliclyAs(
                        'board',
                        $board->getTable() . "_" . $board->id . "_" . $file->hashName()
                    );

                    if (!$fileNameWithPath) {
                        throw new Exception(500, '파일 업로드 실패');
                    }

                    $this->boardUploadService->setUser($request->user());
                    $this->boardUploadService->setFile($file, $fileName);
                    $this->boardUploadService->setCategory('board');

                    if (!$board->files()->save($this->boardUploadService->getFile())) {
                        throw new Exception(500, '파일 정보 저장 실패');
                    }
                }
            }
            DB::commit();

            return redirect()->route('boards.show', $board);
        } catch (Exception $e) {
            DB::rollBack();
            report($e);

            return redirect()->back();
        }
    }

    /**
     * @param Board $board
     * @return Application|Factory|View
     */
    public function show(Board $board)
    {
        return view('boards.show', compact('board'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Board $board
     * @return Response
     */
    public function edit(Board $board): Response
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Board $board
     * @return Response
     */
    public function update(Request $request, Board $board): Response
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Board $board
     * @return Response
     */
    public function destroy(Board $board): Response
    {
        //
    }
}
